#pragma once

#include "point_detector.h"
#include "line_detector.h"
#include "plane_detector.h"

namespace srrg_sashago {

  class ShapeDetector {
  public:

    struct Config {
      float min_distance;
      float max_distance;
      float max_curvature;
      int col_gap;
      int row_gap;
      int normals_blur;
      bool detect_points;
      bool detect_lines;
      bool detect_planes;

      VerbosityLevel verbosity;

      Config() {
        min_distance = 0.05f;
        max_distance = 5.0f;
        max_curvature = 1e-2f;
        col_gap = 5;
        row_gap = 5;
        normals_blur = 3;
        detect_points  = true;
        detect_lines   = true;
        detect_planes  = true;

        verbosity = VerbosityLevel::Info;
      }
    };

    struct TimeStats {
      double detect;
      double point_detection;
      double line_detection;
      double plane_detection;

      TimeStats() {
        setZero();
      }

      inline void setZero() {
        detect = 0.0;
        point_detection = 0.0;
        line_detection = 0.0;
        plane_detection = 0.0;
      }

      friend std::ostream& operator<<(std::ostream& os_, const TimeStats& stats_) {
        os_ << "[BagashaDetector::TimeStats]| total=" << stats_.detect
            << "\tpoint=" << stats_.point_detection
            << "\tline =" << stats_.line_detection
            << "\tplane=" << stats_.plane_detection;
        return os_;
      }

    };
    
    //! @brief ctor/dtor
    ShapeDetector();
    virtual ~ShapeDetector();

    //! @brief configuration
    inline const Config& config() const {return _configuration;}
    inline Config& mutableConfig(){return _configuration;}

    //! @brief inline set/get methods
    inline const TimeStats& timeStats() const {return _time_stats;}
    inline const bool isCameraMatrixSet() const {return _is_camera_matrix_set;}
    //ia this is a shit
    inline PointDetector& pointDetector() {return _point_detector;}
    inline LineDetector& lineDetector() {return _line_detector;}
    inline PlaneDetector& planeDetector() {return _plane_detector;}

    inline const Eigen::Matrix3f& K() const {
      if (!_is_camera_matrix_set) throw std::runtime_error("[BagashaDetector::K]| camera matrix is not set yet, exit");
      return _K;
    }

    inline void setK(const Eigen::Matrix3f& K_){_K = K_;_is_camera_matrix_set = true;}
    inline void setRowsAndCols(const int& rows_, const int& cols_){
      _rows=rows_;
      _cols=cols_;
      _point_detector.setRowsAndCols(rows_, cols_);
    }
    
    //! @brief initializes the things
    void init();

    //! @brief computes a matchable scene and, if provided, fill a hbst entry vector
    void compute(Scene* scene_);
    void setImages(const srrg_core::RawDepthImage& raw_depth_image_,
                   const srrg_core::RGBImage& rgb_image_ = srrg_core::RGBImage());

    inline const srrg_core::Float3Image& normalsImage() const {return _normals_image;}
    inline const srrg_core::Float3Image& directionsImage() const {return _directions_image;}
    inline const srrg_core::Float3Image& pointsImage() const {return _points_image;}
    inline const srrg_core::IntImage& regionsImage() const {return _regions_image;}

//    inline const srrg_core::RGBImage& detectionsImage() const {return _detections_image;}

  protected:
    void computeRegions();
//    void drawDetections(srrg_bagasha::Scene& matchables_);
    
    //ia where do I put those modules???
    PointDetector _point_detector;
    LineDetector  _line_detector;
    PlaneDetector _plane_detector;

    int _rows;
    int _cols;
    Eigen::Matrix3f _K;
    bool _is_camera_matrix_set = false;

    srrg_core::RGBImage _rgb_image;
    srrg_core::FloatImage _depth_image;
    srrg_core::Float3Image _directions_image;
    srrg_core::Float3Image _normals_image;
    srrg_core::Float3Image _cross_normals_image;
    srrg_core::Float3Image _points_image;
    srrg_core::FloatImage _curvature_image;
    srrg_core::IntImage _regions_image;
    srrg_core::UnsignedCharImage _gray_image;
//    srrg_core::RGBImage _detections_image;

    Config _configuration;
    TimeStats _time_stats;
    bool _initialized;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };


} //ia end namespace srrg_bagasha
