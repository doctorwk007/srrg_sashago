#pragma once

#include <opencv/cv.h>
#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#ifdef SRRG_OPENCV_CONTRIB_FOUND
#include <opencv2/xfeatures2d.hpp>
#endif

#include "types/scene.h"

namespace srrg_sashago {

  class PointDetector{
  public:

    typedef std::pair<size_t, size_t> SizeTPair;
    
    
    struct Config {
      int detector_threshold_minimum;
      real detector_threshold_maximum_change;
      size_t detector_threshold_maximum;
      size_t vertical_detectors;
      size_t horizontal_detectors;
      size_t bin_size_pixels;
      real target_number_of_keypoints_tolerance;
      bool nonmax_suppression;
      bool bin_map_enabled;
      std::string descriptors_type;
      
      Config() {
        detector_threshold_minimum = 5;
        vertical_detectors = 1;
        horizontal_detectors = 1;
        bin_size_pixels = 16;
        bin_map_enabled = true;
        target_number_of_keypoints_tolerance = 0.1;
        detector_threshold_maximum_change = 1.0;
        detector_threshold_maximum = 100;
        nonmax_suppression = true;
        descriptors_type = "ORB"; //{ORB, ORB-MOD, BRIEF}
      }
    };

    PointDetector();
    virtual ~PointDetector();
    
    //! @brief inline set/get methods
    const Config& config() const {return _configuration;}
    Config& mutableConfig() {return _configuration;}

    //! @brief intialize all the things
    void init();

    //! @brief set rows and cols of the images
    void setRowsAndCols(const size_t& rows_, const size_t& cols_){
      _image_rows = rows_;
      _image_cols = cols_;
    }
    
    //! @brief given 2 point clouds and an rgb image
    //!        fills the scene with points matchable
    //!        and, if provided, a hbst entry vector
    void compute(Scene* scene_,
                 const srrg_core::RGBImage& rgb_image_,
                 const srrg_core::Float3Image& points_image_,
                 const srrg_core::Float3Image& normals_image_);

  protected:

    void _detectKeypoints(const srrg_core::RGBImage& rgb_image_,
                          std::vector<cv::KeyPoint>& keypoints_);
    void _adjustDetectorThresholds();
    void _initDetectors();
    
    cv::Ptr<cv::FastFeatureDetector>** _detectors = nullptr;    
    cv::Ptr<cv::DescriptorExtractor> _descriptor_extractor;

    //! @brief point detection is made on image regions
    //! @brief with dynamic thresholds
    real** _detector_thresholds = nullptr;
    size_t _number_of_detectors;
    cv::Rect** _detector_regions = nullptr;


    size_t _number_of_cols_bin;
    size_t _number_of_rows_bin;
    size_t _target_number_of_keypoints;
    size_t _target_number_of_keypoints_per_detector;

    size_t _image_rows;
    size_t _image_cols;
    
    Config _configuration;
    bool _initialized;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };
  

} //ia end namespace srrg_bagasha
