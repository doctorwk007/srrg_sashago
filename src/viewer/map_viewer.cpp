#include <viewer/map_viewer.h>

#include "srrg_gl_helpers/opengl_primitives.h"

namespace srrg_sashago {

  Matrix3 MapViewer::_plane_rotation_offset = Matrix3::Identity();
  Isometry3 MapViewer::_camera_offset = Isometry3::Identity();

  MapViewer::MapViewer(QWidget* parent_) :
          QGLViewer(parent_),
          _camera(0) {
    _termination_requested = false;
    _pause_playback = false;
    _step_mode = false;
    _num_steps = 0;

    _plane_rotation_offset << 0, 0, 1,
        -1, 0, 0,
        0, -1, 0;

    Matrix3 rot;
    rot = AngleAxis(-M_PI, Vector3::UnitX());
    _camera_offset.linear() = rot;

  }

  MapViewer::~MapViewer() {
    _map = 0;
  }


  void MapViewer::update() {
    if (!_qapp_ptr)
      throw std::runtime_error("[BagashaViewer::update]| invalid qglserver, exit");

    if (!isVisible())
      return;
    
    _mtx_data_exchange.lock();
    updateGL();
    _mtx_data_exchange.unlock();
    _qapp_ptr->processEvents();

    if(_num_steps)
      --_num_steps;
  }


  QString MapViewer::helpString() const {
    QString text("<h2>BagashaViewer</h2>");
    text += "Check the <b>Keyboard</b> tab for keyboard commands\n";
    text += "Press <b>ESC</b> to quit.";
    return text;
  }

  void MapViewer::init() {
    QGLViewer::init();
    glClearColor(1.0f,1.0f,1.0f,1.0f);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_FLAT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_LIGHTING);

    // Don't save state.
    setStateFileName(QString::null);

    // Mouse bindings.
    setMouseBinding(Qt::NoModifier, Qt::RightButton, CAMERA, ZOOM);
    setMouseBinding(Qt::NoModifier, Qt::MidButton, CAMERA, TRANSLATE);
    setMouseBinding(Qt::ControlModifier, Qt::LeftButton, RAP_FROM_PIXEL);

    //ia key description
    setKeyDescription(Qt::Key_1, "Toggles map points display");
    setKeyDescription(Qt::Key_2, "Toggles trajectory display at maximum granularity");
    setKeyDescription(Qt::Key_3, "Decreases granularity of trajectory display by factor 2");
    setKeyDescription(Qt::Key_4, "Increases granularity of trajectory display by factor 2");
    setKeyDescription(Qt::Key_5, "Decreases camera size by factor 2");
    setKeyDescription(Qt::Key_6, "Increases camera size by factor 2");
    setKeyDescription(Qt::Key_7, "Decreases landmark size by factor 2");
    setKeyDescription(Qt::Key_8, "Increases landmark size by factor 2");
    setKeyDescription(Qt::Key_9, "Toggle visualization of last scene");
    setKeyDescription(Qt::Key_Space, "Play/pause processing");
    setKeyDescription(Qt::Key_C, "Toggles follow camera mode");
    setKeyDescription(Qt::Key_F, "Toggles increment by 1 step the processing queue");
    setKeyDescription(Qt::Key_G, "Toggles gt trajectory visualization - if present");
    setKeyDescription(Qt::Key_S, "Toggles step mode");
    setKeyDescription(Qt::Key_T, "Toggles trajectory visualization");

    // Replace camera.
    qglviewer::Camera *oldcam = camera();
    _camera = new StandardCamera();
    setCamera(_camera);
    
    _camera->setPosition(qglviewer::Vec(0.0f, 0.0f, 10.0f));
    _camera->setUpVector(qglviewer::Vec(0.0f, 0.0f, 0.0f));
    _camera->lookAt(qglviewer::Vec(0.0f, 0.0f, 0.0f));
    delete oldcam;
  }

  void MapViewer::draw() {
    if (!_map)
      throw std::runtime_error("[BagashaViewer::init]| no valid map, did you forget to call \'setMap()\'?");
    if (!_scenes)
      throw std::runtime_error("[BagashaViewer::init]| no valid scene container, did you forget to call \'setSceneContainer()\'?");

    glPushMatrix();
    srrg_gl_helpers::glMultMatrix(_camera_offset);

    if(_configuration.follow_camera)
      srrg_gl_helpers::glMultMatrix(_scenes->at(_scenes->size()-1)->pose().inverse());

    if (_configuration.draw_landmarks)
      _drawMapLandmarks();

    if (_configuration.draw_full_camera_track)
      _drawDenseCameraTrack();
    else
      _drawSparseCameraTrack();
    
    if (_configuration.draw_trajectory)
      _drawCameraTrack();

    if (_configuration.draw_gt)
      _drawGTCameraTrack();

    if (_configuration.draw_last_scene)
      _drawScene(_scenes->at(_scenes->size()-1));

    glPopMatrix();
    return;
  }

  void MapViewer::keyPressEvent(QKeyEvent* event_) {
     switch (event_->key()) {
     case Qt::Key_C:
     {
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| follow camera Mode toggled" << std::endl;
       _configuration.follow_camera ^= 1;
       break;
     }
     case Qt::Key_S:
     {
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| step Mode toggled" << std::endl;
       _step_mode = !_step_mode;
       if (_step_mode)
         _num_steps = 0;
       break;
     }
     case Qt::Key_F:
     {
       ++_num_steps;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| step Mode - num steps: " << _num_steps << std::endl;
       break;
     }     
     case Qt::Key_T:
     {
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| toggle trajectory visualization: " << std::endl;
       _configuration.draw_trajectory = !_configuration.draw_trajectory;
       break;
     }
     case Qt::Key_G:
     {
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| toggle ground truth trajectory visualization: " << std::endl;
       _configuration.draw_gt = !_configuration.draw_gt;
       break;
     }
     case Qt::Key_Escape:
     {
       _termination_requested = true;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| termination requested" << std::endl;
       break;
     }

     //ia toggle landmark visualization
     case Qt::Key_1:
     {
       if(_configuration.draw_landmarks) {
         _configuration.draw_landmarks = false;
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| landmarks drawn - disabled" << std::endl;
       }
       else {
         _configuration.draw_landmarks = true;
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| landmarks drawn - enabled" << std::endl;
       }
       break;
     }
     //ia toggles maximum granularity of camera track
     case Qt::Key_2:
     {
       if(_configuration.draw_full_camera_track) {
         _configuration.draw_full_camera_track = false;
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| camera track visualization at maximum granularity - disabled" << std::endl;
       }
       else {
         _configuration.draw_full_camera_track = true;
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| camera track visualization at maximum granularity - enabled" << std::endl;
       }
       break;
     }

     //ia decrease camera track granularity
     case Qt::Key_3:
     {
       _configuration.camera_track_distance = _configuration.camera_track_distance/2;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| decreasing camera granularity by factor 2" << std::endl;
       break;
     }

     //ia increase camera track granularity
     case Qt::Key_4:
     {
       _configuration.camera_track_distance = _configuration.camera_track_distance*2;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| increasing camera granularity by factor 2" << std::endl;
       break;
     }

     //ia decrease camera size
     case Qt::Key_5:
     {
       _configuration.camera_wf_width = _configuration.camera_wf_width/2.0f;
       _configuration.camera_wf_height = _configuration.camera_wf_height/2.0f;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| decreasing camera size by factor 2" << std::endl;
       break;
     }

     //ia increase camera size
     case Qt::Key_6:
     {
       _configuration.camera_wf_width = _configuration.camera_wf_width*2.0f;
       _configuration.camera_wf_height = _configuration.camera_wf_height*2.0f;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| increasing camera size by factor 2" << std::endl;
       break;
     }

     //ia decrease matchable size
     case Qt::Key_7:
     {
       _configuration.point_size = _configuration.point_size/2.0f;
       _configuration.line_width = _configuration.line_width/2.0f;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| decreasing landmark size by factor 2" << std::endl;
       break;
     }

     //ia increase matchable size
     case Qt::Key_8:
     {
       _configuration.point_size = _configuration.point_size*2.0f;
       _configuration.line_width = _configuration.line_width*2.0f;
       if (_configuration.verbosity > VerbosityLevel::None)
         std::cerr << "[BagashaViewer::keyPressEvent]| increasing landmark size by factor 2" << std::endl;
       break;
     }

     //ia show last frame visualization
     case Qt::Key_9:
     {
       if(_configuration.draw_last_scene) {
         _configuration.draw_last_scene = false;
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| last scene visualization - disabled" << std::endl;
       }
       else {
         _configuration.draw_last_scene = true;
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| last scene - enabled" << std::endl;
       }
       break;
     }

     case Qt::Key_Space:
     {
       _pause_playback = !_pause_playback;
       if (!_pause_playback) {
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| resuming playback" << std::endl;
       } else {
         if (_configuration.verbosity > VerbosityLevel::None)
           std::cerr << "[BagashaViewer::keyPressEvent]| pausing playback" << std::endl;
       }
       break;
     }
    
     default:
       QGLViewer::keyPressEvent(event_);

    }
     draw();
     updateGL();

  }

  const Eigen::Vector3f MapViewer::_computeMatchableColor(SceneEntry* entry_) const {
    Eigen::Vector3f color_rgb = Eigen::Vector3f::Ones();

    switch (entry_->matchable().type()) {
    case MatchableBase::Type::Point:
    {
      if (entry_->landmark())
        color_rgb = VIEWER_COLOR_RED;
      else
        color_rgb = VIEWER_COLOR_DARK_RED;
      break;
    }
    case MatchableBase::Type::Line:
    {
      if (entry_->landmark())
        color_rgb = VIEWER_COLOR_GREEN;
      else
        color_rgb = VIEWER_COLOR_DARK_GREEN;
      break;
    }
    case MatchableBase::Type::Plane:
    {
      if (entry_->landmark())
        color_rgb = VIEWER_COLOR_BLUE;
      else
        color_rgb = VIEWER_COLOR_DARK_BLUE;
      break;
    }
    default:
      throw std::runtime_error("[BagashaViewer::_computeMatchableColor]| unknown matchable type");
    }

    return color_rgb;
  }

  void MapViewer::_drawMapLandmarks() const {
    WorldMap::LandmarkContainer::const_iterator m_it = _map->landmarks().begin();
    WorldMap::LandmarkContainer::const_iterator m_end = _map->landmarks().end();

    while (m_it != m_end) {
      SceneEntry* entry = (*m_it)->entry();
      const Eigen::Vector3f color = _computeMatchableColor(entry);
      _drawSceneEntry(entry, color);
      ++m_it;
    }
  }

  void MapViewer::_drawDenseCameraTrack() const {
    IntSceneMap::const_iterator s_it = _scenes->begin();
    IntSceneMap::const_iterator s_end = _scenes->end();

    glPushAttrib(GL_COLOR|GL_POINT_SIZE|GL_LINE_WIDTH);
    glColor4f(_configuration.camera_wf_color.x(),_configuration.camera_wf_color.y(), _configuration.camera_wf_color.z(), 1.f);
    while (s_it != s_end) {
      if (s_it->second->isSet()) {
        _drawCamera(s_it->second->pose());
      }
      ++s_it;
    }
    glPopAttrib();
  }

  void MapViewer::_drawCameraTrack() const {
    IntSceneMap::const_iterator s_it = _scenes->begin();
    IntSceneMap::const_iterator s_end = _scenes->end();
    if(s_it == s_end)
      return;
    IntSceneMap::const_iterator s_next = std::next(s_it,1);

    while(s_next != s_end) {
      if (!s_it->second->isSet())
        continue;
      glPushMatrix();
      const Vector3& start_pose = s_it->second->pose().translation();
      const Vector3& end_pose = s_next->second->pose().translation();
      glBegin(GL_LINES);
      {
        glColor4f(_configuration.camera_wf_color.x(),_configuration.camera_wf_color.y(), _configuration.camera_wf_color.z(), 0.2f);
        glVertex3f(start_pose.x(), start_pose.y(), start_pose.z());
        glVertex3f(end_pose.x(), end_pose.y(), end_pose.z());
      }
      glEnd();
      glPopMatrix();
      ++s_next;
      ++s_it;
    }
  }
  
  void MapViewer::_drawGTCameraTrack() const {
    IntSceneMap::const_iterator s_it = _scenes->begin();
    IntSceneMap::const_iterator s_end = _scenes->end();

    glPushAttrib(GL_COLOR|GL_POINT_SIZE|GL_LINE_WIDTH);
    glColor4f(_configuration.gt_color.x(),_configuration.gt_color.y(), _configuration.gt_color.z(), 1.f);
    while (s_it != s_end) {
      _drawCamera(s_it->second->gtPose());
      ++s_it;
    }
    glPopAttrib();
  }


  void MapViewer::_drawSparseCameraTrack() const {
    IntSceneMap::const_iterator s_it = _scenes->begin();
    IntSceneMap::const_iterator s_end = _scenes->end();

    glPushAttrib(GL_COLOR|GL_POINT_SIZE|GL_LINE_WIDTH);
    glColor4f(_configuration.camera_wf_color.x(),_configuration.camera_wf_color.y(), _configuration.camera_wf_color.z(), 1.f);
    while (s_it != s_end) {
      if (s_it->first % _configuration.camera_track_distance == 0) {
        if (s_it->second->isSet())
          _drawCamera(s_it->second->pose());
      }
      ++s_it;
    }
    glPopAttrib();
  }

  void MapViewer::_drawScene(Scene* scene_) const {
    for (size_t i = 0; i < scene_->entries().size(); ++i) {
      SceneEntry* entry = scene_->entries()[i];

      const Eigen::Vector3f color = _computeMatchableColor(entry);
      _drawSceneEntry(entry, (Eigen::Vector3f::Ones() - color)*0.9);
    }
  }


  void MapViewer::_drawCamera(const Isometry3& transform_) const {
    glPushMatrix();
    srrg_gl_helpers::glMultMatrix(transform_);
    srrg_gl_helpers::drawPyramidWireframe(_configuration.camera_wf_height, _configuration.camera_wf_width);
    glPopMatrix();

  }


  void MapViewer::_drawSceneEntry(SceneEntry* entry_,
                                      const Vector3& color_rgb_) const {
    const Matchable& matchable = entry_->globalMatchable();
    const MatchableBase::Type& m_type = matchable.type();

    switch (m_type) {
    case MatchableBase::Type::Point:
    {
      _drawPoint(matchable.point, color_rgb_);
      break;
    }
    case MatchableBase::Type::Line:
    {
      const float length = (entry_->extent().x() > 0.f) ? entry_->extent().x() : 5;
      Vector3 end_point = matchable.point + matchable.directionVector() * length;
      _drawLine(matchable.point, end_point, color_rgb_);
      break;
    }
    case MatchableBase::Type::Plane:
    {
      if (_configuration.draw_simple_planes) {
        _drawSimplePlane(matchable.point, matchable.rotation, color_rgb_);
      } else {
        _drawPlane(entry_->cloud(), matchable.point, matchable.rotation, color_rgb_);
      }
      break;
    }
    default:
      throw std::runtime_error("[MatchableViewer] unknown matchable type");
    }
  }

  void MapViewer::_drawPoint(const Vector3& p_,
                                 const Eigen::Vector3f& color_rgb_) const {
    glPushAttrib(GL_COLOR|GL_POINT_SIZE|GL_LINE_WIDTH);
    glPointSize(_configuration.point_size);
    glColor4f(color_rgb_.x(),color_rgb_.y(), color_rgb_.z(), 1.f);
    glBegin(GL_POINTS);
    glVertex3f(p_.x(), p_.y(), p_.z());
//
//    Vector3 n = -p_.normalized();

//    glNormal3f(n.x(),n.y(),n.z());
    glEnd();
    glPopAttrib();
  }

  void MapViewer::_drawLine(const Vector3& p0_,
                                const Vector3& p1_,
                                const Eigen::Vector3f& color_rgb_) const {
    glPushAttrib(GL_COLOR|GL_POINT_SIZE|GL_LINE_WIDTH);
    glLineWidth(_configuration.line_width);
    glColor4f(color_rgb_.x(),color_rgb_.y(), color_rgb_.z(), 1.f);
    glBegin(GL_LINES);
    glVertex3f(p0_.x(), p0_.y(), p0_.z());
    glVertex3f(p1_.x(), p1_.y(), p1_.z());
    glEnd();
    glPopAttrib();
  }

  void MapViewer::_drawSimplePlane(const Vector3& center_,
      const Matrix3& rotation_,
      const Eigen::Vector3f& color_rgb_) const {
    Eigen::Isometry3f transform;
    transform.translation()=center_;
    transform.linear()=rotation_;

    glPushAttrib(GL_COLOR|GL_POINT_SIZE|GL_LINE_WIDTH);
    glPushMatrix();
    srrg_gl_helpers::glMultMatrix(transform);
    glColor4f(color_rgb_.x(),color_rgb_.y(), color_rgb_.z(), 1.f);

    glBegin(GL_QUADS);
    glVertex3f(0,0.05,0.05);
    glVertex3f(0,-0.05,0.05);
    glVertex3f(0,-0.05,-0.05);
    glVertex3f(0,0.05,-0.05);
    glNormal3f(0,0,1);
    glEnd();

    glPopMatrix();
    glPopAttrib();
  }

  void MapViewer::_drawPlane(const srrg_core::Cloud3D& plane_cloud_,
      const Vector3& center_,
      const Matrix3& rotation_,
      const Eigen::Vector3f& color_rgb_) const {
    Eigen::Isometry3f transform;
    transform.translation()=center_;
    transform.linear()=rotation_  * _plane_rotation_offset;

    glPushAttrib(GL_COLOR|GL_POINT_SIZE|GL_LINE_WIDTH);
    glPushMatrix();
    srrg_gl_helpers::glMultMatrix(transform);
    glColor4f(color_rgb_.x(),color_rgb_.y(), color_rgb_.z(), 0.7f);

    plane_cloud_.draw(0);

    glPopMatrix();
    glPopAttrib();
  }



} /* namespace srrg_bagasha */
