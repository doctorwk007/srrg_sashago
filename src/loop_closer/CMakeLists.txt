add_library(srrg_sashago_loop_closer_library SHARED
  loop_closer.h
  loop_closer.cpp)

target_link_libraries(srrg_sashago_loop_closer_library
  srrg_image_utils_library
  srrg_sashago_types_library
  srrg_sashago_aligner_library
  ${SRRG_G2O_LIBRARIES}
  ${CSPARSE_LIBRARY}
  ${CHOLMOD_LIBRARY})
  