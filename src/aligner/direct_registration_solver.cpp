#include "direct_registration_solver.h"
#include <iostream>

#include <Eigen/Cholesky>
#include <Eigen/SVD>

namespace srrg_sashago {

  Vector12 DirectRegistrationSolver::flattenIsometry(const Isometry3& iso){
    Vector12 v=Vector12::Zero();
    v.block<3,1>(0,0)=iso.translation();
    v.block<3,1>(3,0)=iso.linear().row(0).transpose();
    v.block<3,1>(6,0)=iso.linear().row(1).transpose();
    v.block<3,1>(9,0)=iso.linear().row(2).transpose();
    return v;
  }

  Isometry3 DirectRegistrationSolver::unflattenIsometry(const Vector12& v) {
    Isometry3 iso=Isometry3::Identity();
    iso.translation()=v.block<3,1>(0,0);
    iso.linear().row(0)=v.block<3,1>(3,0).transpose();
    iso.linear().row(1)=v.block<3,1>(6,0).transpose();
    iso.linear().row(2)=v.block<3,1>(9,0).transpose();
    return iso;
  }

  Matrix6_12 DirectRegistrationSolver::jacobianT(const Vector3& point,
                                                 const Vector3& direction){
    Matrix6_12 Jt=Matrix6_12::Zero();
    Jt.block<3,3>(0,0).setIdentity();
    Jt.block<1,3>(0,3)=point.transpose();
    Jt.block<1,3>(1,6)=point.transpose();
    Jt.block<1,3>(2,9)=point.transpose();
    Jt.block<1,3>(3,3)=direction.transpose();
    Jt.block<1,3>(4,6)=direction.transpose();
    Jt.block<1,3>(5,9)=direction.transpose();
    return Jt;
  }

  DirectRegistrationSolver::DirectRegistrationSolver() {
    _H.setZero();
    _b.setZero();
  }

  DirectRegistrationSolver::~DirectRegistrationSolver() { }

  // computes the error and the Jacobian of a constraint
  void DirectRegistrationSolver::errorAndJacobian(Vector3& e_p,    // point-error
                                                  Vector3& e_d,    // direction-error
                                                  real& e_o,              // orthogonality error
                                                  real& e_x,              // intersection error
                                                  Matrix3_12& J_p,  // point jacobian
                                                  Matrix3_12& J_d,  // direction jacobian
                                                  Matrix1_12& J_o,  // orthogonality jacobian
                                                  Matrix1_12& J_x,  // orthogonality error
                                                  bool compute_p,          // if true computes the point part
                                                  bool compute_d,          // if true computes the direction part
                                                  bool compute_o,          // if true computes the orthogonality part
                                                  bool compute_x,          // if true computes the orthogonality part
                                                  const Matchable& fixed,  // input fixed item
                                                  const Matchable& moving // input moving item
                                                  )
  {

    Matrix6_12 J_t = jacobianT(moving.point, moving.directionVector());

    // transform the moving item and put direction point and direction vectors
    const Vector3& p_fixed = fixed.point;
    const Matrix3& R_fixed = fixed.rotation;
    const Vector3& d_fixed = fixed.directionVector();
    const Vector3 p_moving = _T*moving.point;
    const Vector3 d_moving = _T.linear()*moving.directionVector();
    const Matrix3 R_fixed_prime = R_fixed.transpose();

    // compute the always useful cross produce terms
    Matrix3 p_moving_cross=srrg_core::skew(p_moving);
    Matrix3 d_moving_cross=srrg_core::skew(d_moving);

    Matrix3_9 J_p_t = Matrix3_9::Zero();
    J_p_t.block<1,3>(0,0)=moving.point.transpose();
    J_p_t.block<1,3>(1,3)=moving.point.transpose();
    J_p_t.block<1,3>(2,6)=moving.point.transpose();

    Matrix3_9 J_d_t = Matrix3_9::Zero();
    J_d_t.block<1,3>(0,0)=moving.directionVector().transpose();
    J_d_t.block<1,3>(1,3)=moving.directionVector().transpose();
    J_d_t.block<1,3>(2,6)=moving.directionVector().transpose();

    if (compute_p){
      // point error and jacobian
      e_p=p_moving-p_fixed;
      //      Matrix3_6 J_pp=Matrix3_6::Zero();
      //      J_pp.block<3,3>(0,0).setIdentity();
      //      J_p=J_pp*J_t;
      J_p.block<3,3>(0,0)=R_fixed_prime;
      J_p.block<3,9>(0,3)=R_fixed_prime*J_p_t;
    }
    // direction error and jacobian
    if (compute_d) {
      e_d=d_moving-d_fixed;
      //      Matrix3_6 J_dd=Matrix3_6::Zero();
      //      J_dd.block<3,3>(0,3).setIdentity();
      //      J_d=J_dd*J_t;
      J_d.block<3,3>(0,0).setZero();
      J_d.block<3,9>(0,3)=J_d_t;
    }
    // orthogonality error and jacobian
    if(compute_o){
      e_o=d_moving.dot(d_fixed);
      //      Matrix1_6f J_oo=Matrix1_6f::Zero();
      //      J_oo.block<1,3>(0,3)=d_fixed.transpose();
      //      J_o=J_oo*J_t;
      J_o.block<1,3>(0,0).setZero();
      J_o.block<1,9>(0,3)=d_fixed.transpose()*J_d_t;
    }
    // intersection error and jacobian
    if(compute_x){
      Vector3 d_moving_cross_d_fixed=d_moving.cross(d_fixed);
      e_x=e_p.dot(d_moving_cross_d_fixed);
      Matrix1_6 J_xx;
      J_xx.block<1,3>(0,0)=d_moving_cross_d_fixed.transpose();
      J_xx.block<1,3>(0,3)=-e_p.transpose()* srrg_core::skew((Vector3)d_fixed);
      J_x=J_xx*J_t;
    }
  }



  void DirectRegistrationSolver::init(const Isometry3& transform_,
                                      const bool use_support_weight) {
    BaseRegistrationSolver::init(transform_, use_support_weight);
    _H.setZero();
    _b.setZero();
  }


  // generic constraint linearization
  // side effect on _H and _b
  void DirectRegistrationSolver::linearizeConstraint(Constraint& constraint) {
    const Matchable& fixed = constraint.fixed->globalMatchable();
    const Matchable& moving = constraint.moving->matchable();

    Matrix3 fixed_weight, moving_weight, constraint_weight;
    moving_weight = constraint.moving->computeWeight();
    fixed_weight  = constraint.fixed->computeWeight();
    constraint_weight = (moving_weight.determinant() > fixed_weight.determinant())?  moving_weight : fixed_weight; 

    // buffer variables to avoid abusing the stack
    Vector3 e_p = Vector3::Zero();
    Vector3 e_d = Vector3::Zero();
    real e_o = 0.0;
    real e_x = 0.0;
    Matrix3_12 J_p = Matrix3_12::Zero();
    Matrix3_12 J_d = Matrix3_12::Zero();
    Matrix1_12 J_o = Matrix1_12::Zero();
    Matrix1_12 J_x = Matrix1_12::Zero();


    // the following variable will contain
    // what we have to do to linearize this constraint
    SelectionMatrixEntry entry;

    // we handle differently the intersection constraint,
    // that is not contained in the action matrix
    if (constraint.is_intersection){
      assert(moving.type()==Matchable::Line&&
             moving.type()==Matchable::Line&&
             "DirectRegistrationSolver::linearizeConstraint(): " &&
             "for an intersection constraint" &&
             "the types should be both lines");
      entry.compute_p=false;
      entry.compute_d=false;
      entry.compute_o=false;
      entry.compute_x=true;
      entry.rotate_omega=false;
    } else {
      // fetch on the action matrix which parts of the jacobians
      // we have to compute
      // and how to deal with the omegas
      entry=_selection_matrix[moving.type()][fixed.type()];
    }

    /*

      cerr << "moving type: " << moving->type() << endl;
      cerr << "fixed type: " << fixed->type() << endl;

      cerr << "entry, selectin_matrix" << endl;
      cerr << "compute_p: "<< entry.compute_p << " ";
      cerr << "compute_d: "<< entry.compute_d << " ";
      cerr << "compute_o: "<< entry.compute_o << " ";
      cerr << "compute_x: "<< entry.compute_x << " ";
      cerr << "rotate:    "<< entry.rotate_omega << endl;;

    */

    //compute the jacobian

    errorAndJacobian(e_p, e_d, e_o, e_x,
                     J_p, J_d, J_o, J_x,
                     entry.compute_p,
                     entry.compute_d,
                     entry.compute_o,
                     entry.compute_x,
                     fixed,
                     moving);
    
    // std::cerr << "ep: " << e_p.transpose() << std::endl;
    // std::cerr << "ed: " << e_d.transpose() << std::endl;
    // std::cerr << "eo: " << e_o << std::endl;
    // std::cerr << "ex: " << e_x << std::endl;
    // std::cerr << "Jp: \n" << J_p << std::endl;
    // std::cerr << "Jd: \n" << J_d << std::endl;
    // std::cerr << "Jo: \n" << J_o << std::endl;
    // std::cerr << "Jx: \n" << J_x << std::endl;
    
    // handle point term
    constraint.resetErrors();
    // temporary variables to hold the updates
    Matrix12 H_temp=Matrix12::Zero();
    Vector12 b_temp=Vector12::Zero();
    bool is_inlier=true;
    if (entry.compute_p){
      Matrix3 omega_p=fixed.omega_p;

      // this is to handle the case when we have to rotate the omega matrix
      // during the iterations
      //      if (!puttana && entry.rotate_omega) {
      //        const Matrix3& R=_T.linear();
      //        omega_p=R*moving->directionMatrix()*R.transpose();
      //      }

      if(_use_support_weight){
        omega_p += constraint_weight;
      }

      constraint.error_p=e_p.transpose()*omega_p*e_p;
      if(constraint.error_p>_point_kernel_threshold) {
        real scale=sqrt(_point_kernel_threshold/constraint.error_p);
        omega_p*=scale;
        is_inlier=false;
      }
      H_temp+=J_p.transpose()*omega_p*J_p;
      b_temp+=J_p.transpose()*omega_p*e_p;
    }
    // handle the direction term
    if (entry.compute_d){
      Matrix3 omega_d=_omega_d;
      if(_use_support_weight){
        omega_d += constraint_weight;
      }
      constraint.error_d=e_d.transpose()*omega_d*e_d;
      if(constraint.error_d>_direction_kernel_threshold) {
        real scale=sqrt(_direction_kernel_threshold/constraint.error_d);
        omega_d*=scale;
        is_inlier=false;
      }
      H_temp+=J_d.transpose()*omega_d*J_d;
      b_temp+=J_d.transpose()*omega_d*e_d;
    }
    // handle the orthogonality term
    if (entry.compute_o){
      real omega_o=_omega_o;
      if(_use_support_weight){
        omega_o += constraint_weight(0,0);
      }
      constraint.error_o=e_o*omega_o*e_o;
      if(constraint.error_o>_orthogonal_kernel_threshold) {
        real scale=sqrt(_orthogonal_kernel_threshold/constraint.error_o);
        omega_o*=scale;
        is_inlier=false;
      }
      H_temp+=J_o.transpose()*omega_o*J_o;
      b_temp+=J_o.transpose()*omega_o*e_o;
    }
    // handle the direction term
    if (entry.compute_x){
      constraint.error_x=e_x*_omega_x*e_x;
      real omega_x=_omega_x;
      if(constraint.error_x>_intersection_kernel_threshold) {
        real scale=sqrt(_intersection_kernel_threshold/constraint.error_x);
        omega_x*=scale;
        is_inlier=false;
      }
      H_temp+=J_x.transpose()*omega_x*J_x;
      b_temp+=J_x.transpose()*omega_x*e_x;
    }

    if (is_inlier || ! _ignore_outliers){     
      _H+=H_temp;
      _b+=b_temp;
    }
  }

  void DirectRegistrationSolver::oneRound(){
    if (_upper_flag)
      oneRoundUpper();
    else
      oneRoundLower();
  }

  void DirectRegistrationSolver::oneRoundUpper(){
    //    cerr << "Solver T (before)" << endl;
    //    cerr << _T.matrix() << endl;
    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H+=Matrix12::Identity()*_damping;
    Vector12 dx=_H.ldlt().solve(-_b);

    Vector12 x=flattenIsometry(_T);
    x+=dx;
    Isometry3 temp_T=unflattenIsometry(x);
    Eigen::JacobiSVD<Matrix3> svd(temp_T.linear(), Eigen::ComputeThinU | Eigen::ComputeThinV);
    temp_T.linear()=svd.matrixU()*svd.matrixV().transpose();
    _T=temp_T;

    // round 2, lock the translation, and solve for the rotation
    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H+=Matrix12::Identity()*_damping;

    Matrix3 Ht=_H.block<3,3>(0,0);
    Vector3 bt=_b.block<3,1>(0,0);
    Vector3 dt=Ht.ldlt().solve(-bt);
    _T.translation()+=dt;
    //    cerr << "Solver T (after)" << endl;
    //    cerr << _T.matrix() << endl;
  }

  void DirectRegistrationSolver::oneRoundLower(){
    //    cerr << "Solver T (before)" << endl;
    //    cerr << _T.matrix() << endl;

    Vector3 y= _T.linear().transpose()*_T.translation();
    Isometry3 T2=_T;
    T2.translation()=y;

    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }

   
    _H+=Matrix12::Identity()*_damping;
    Vector12 dx=_H.ldlt().solve(-_b);

    Vector12 x=flattenIsometry(T2);
    x+=dx;
    Isometry3 temp_T=unflattenIsometry(x);


    // bdc, this cpy shit is due to JacobiSVD that works
    //      only with Dynamic Matrices
    //      n.b. standard usage (with Matrix3) may work in Release mode
    //           but not in debug
    MatrixX temp_t_linear = temp_T.linear();   
    Eigen::JacobiSVD<MatrixX> svd(temp_t_linear, Eigen::ComputeThinU | Eigen::ComputeThinV);
    /**/
    
    // bdc, this works only in Release mode. For debug, see above (and say 'thanks' to Eigen lib)
//    Eigen::JacobiSVD<Matrix3> svd(temp_T.linear(), Eigen::ComputeThinU | Eigen::ComputeThinV);
    
    temp_T.linear()=svd.matrixU()*svd.matrixV().transpose();

    _T=temp_T;
    _T.translation()=_T.linear()*_T.translation();

    // round 2, lock the translation, and solve for the rotation
    y= _T.linear().transpose()*_T.translation();
    T2=_T;
    T2.translation()=y;

    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H+=Matrix12::Identity()*_damping;

    Matrix3 Ht = _H.block<3,3>(0,0);
    Vector3 bt = _b.block<3,1>(0,0);
    Vector3 dt = Ht.ldlt().solve(-bt);
    T2.translation() += dt;

    _T=T2;
    _T.translation()=_T.linear()*_T.translation();

    //    cerr << "Solver T (after)" << endl;
    //    cerr << _T.matrix() << endl;
  }

} //ia end namespace srrg_bagasha
