#include "iterative_registration_solver.h"

#include <Eigen/Cholesky>

namespace srrg_sashago {

  IterativeRegistrationSolver::IterativeRegistrationSolver() {
    _H.setZero();
    _b.setZero();

    _chi_square = 0;
    _num_inliers = 0;
  }

  IterativeRegistrationSolver::~IterativeRegistrationSolver() { }

  // computes the error and the Jacobian of a constraint
  void IterativeRegistrationSolver::errorAndJacobian(Vector3& e_p,    // point-error
                                                     Vector3& e_d,    // direction-error
                                                     real& e_o,              // orthogonality error
                                                     real& e_x,              // intersection error
                                                     Matrix3_6& J_p,  // point jacobian
                                                     Matrix3_6& J_d,  // direction jacobian
                                                     Matrix1_6& J_o,  // orthogonality jacobian
                                                     Matrix1_6& J_x,  // orthogonality error
                                                     bool compute_p,          // if true computes the point part
                                                     bool compute_d,          // if true computes the direction part
                                                     bool compute_o,          // if true computes the orthogonality part
                                                     bool compute_x,          // if true computes the orthogonality part
                                                     const Matchable& fixed,  // input fixed item
                                                     const Matchable& moving, // input moving item
                                                     const bool rotate_omega)
  {
    // transform the moving item and put direction point and direction vectors
    const Vector3& p_fixed = fixed.point;
    const Matrix3& R_fixed = fixed.rotation;
    const Vector3& d_fixed = fixed.directionVector();
    const Vector3 p_moving = _T*moving.point;
    const Matrix3 R_moving = _T.linear()*moving.rotation;
    const Vector3 d_moving = _T.linear()*moving.directionVector();
    const Matrix3 R_fixed_prime = R_fixed.transpose();
    const Matrix3 R_moving_prime= moving.rotation.transpose();
    
    // compute the always useful cross produce terms
    Matrix3 p_moving_cross=srrg_core::skew(p_moving);
    Matrix3 d_moving_cross=srrg_core::skew(d_moving);
    Matrix3 d_fixed_cross =srrg_core::skew(d_fixed);

    // point error and jacobian
    e_p = R_fixed_prime * (p_moving - p_fixed);
    
    if (compute_p){ 
      J_p.block<3,3>(0,0)=R_fixed_prime;
      J_p.block<3,3>(0,3)=-R_fixed_prime*p_moving_cross;
    }

    // direction error and jacobian
    if (compute_d) {
      e_d=d_moving-d_fixed;
      J_d.block<3,3>(0,0).setZero();
      J_d.block<3,3>(0,3)=-d_moving_cross;
    }

    // orthogonality error and jacobian
    if(compute_o){     
      e_o=d_moving.dot(d_fixed);
      J_o.setZero();
      J_o.block<1,3>(0,3)=(R_moving_prime*_T.linear().transpose()*d_fixed_cross).row(2);
    }

    // intersection error and jacobian
    if(compute_x){
      Eigen::Vector3f d_moving_cross_d_fixed=d_moving.cross(d_fixed);
      e_x=e_p.dot(d_moving_cross_d_fixed);
      J_x.block<1,3>(0,0)=-d_moving_cross_d_fixed.transpose() * p_moving_cross;
      J_x.block<1,3>(0,3)=e_p.transpose()* srrg_core::skew((Eigen::Vector3f)d_fixed)*d_moving_cross;
    }
  }

  // generic constraint linearization
  // side effect on _H and _b
  void IterativeRegistrationSolver::linearizeConstraint(Constraint& constraint) {

    const Matchable& fixed = constraint.fixed->globalMatchable();
    const Matchable& moving = constraint.moving->matchable();

    Matrix3 fixed_weight, moving_weight, constraint_weight;
    fixed_weight = constraint.fixed->computeWeight();
    moving_weight = constraint.moving->computeWeight();
    constraint_weight = (moving_weight.determinant() > fixed_weight.determinant())?  moving_weight : fixed_weight; 

    // buffer variables to avoid abusing the stack
    Vector3 e_p = Vector3::Zero();
    Vector3 e_d = Vector3::Zero();

    real e_o = 0.f;
    real e_x = 0.f;

    Matrix3_6 J_p = Matrix3_6::Zero();
    Matrix3_6 J_d = Matrix3_6::Zero();
    Matrix1_6 J_o = Matrix1_6::Zero();
    Matrix1_6 J_x = Matrix1_6::Zero();

    // the following variable will contain
    // what we have to do to linearize this constraint
    SelectionMatrixEntry entry;

    // we handle differently the intersection constraint,
    // that is not contained in the action matrix
    if (constraint.is_intersection){
      assert(moving.type() == Matchable::Type::Line&&
             moving.type() == Matchable::Type::Line&&
             "IterativeRegistrationSolver::linearizeConstraint(): " &&
             "for an intersection constraint" &&
             "the types should be both lines");
      entry.compute_p=false;
      entry.compute_d=false;
      entry.compute_o=false;
      entry.compute_x=true;
      entry.rotate_omega=false;
    } else {
      // fetch on the action matrix which parts of the jacobians
      // we have to compute
      // and how to deal with the omegas
      //ia remember -> type == dof
      entry=_selection_matrix[moving.type()][fixed.type()];
    }
    
    //compute the jacobian
    errorAndJacobian(e_p, e_d, e_o, e_x,
                     J_p, J_d, J_o, J_x,
                     entry.compute_p,
                     entry.compute_d,
                     entry.compute_o,
                     entry.compute_x,
                     fixed,
                     moving,
                     entry.rotate_omega);

    // std::cerr << "ep : " << e_p.transpose() << std::endl;
    // std::cerr << "ed : " << e_d.transpose() << std::endl;
    // std::cerr << "eo : " << e_o << std::endl;
    // std::cerr << "ex : " << e_x << std::endl;


    // std::cerr << "Jp :\n" << J_p << std::endl;
    // std::cerr << "Jd :\n" << J_d << std::endl;
    // std::cerr << "Jo :\n" << J_o << std::endl;
    // std::cerr << "Jx :\n" << J_x << std::endl;
    
    // handle point term
    constraint.resetErrors();

    // temporary variables to hold the updates
    Matrix6 H_temp = Matrix6::Zero();
    Vector6 b_temp = Vector6::Zero();

    bool is_inlier=true;
    if (entry.compute_p) {
//      Matrix3f omega_p=fixed->directionMatrix();
      Matrix3 omega_p = fixed.omega_p;

      // this is to handle the case when we have to rotate the omega matrix
      // during the iterations
      /*if (entry.rotate_omega) {
        const Eigen::Matrix3f& R=_T.linear();
        omega_p=R*moving->directionMatrix()*R.transpose();
      }
      */
      if(_use_support_weight  && (fixed.type() == Matchable::Type::Point)){
        omega_p += constraint_weight;
      }

      constraint.error_p=e_p.transpose()*omega_p*e_p;
      if(constraint.error_p > _point_kernel_threshold) {
        real scale=sqrt(_point_kernel_threshold/constraint.error_p);
        omega_p*=scale;
        is_inlier=false;
      }
      
      H_temp+=J_p.transpose()*omega_p*J_p;
      b_temp+=J_p.transpose()*omega_p*e_p;
    }
    //    std::cerr << "P:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;
    
    // handle the direction term
    if (entry.compute_d){

      Eigen::Matrix3f omega_d=_omega_d;
      if(_use_support_weight && (fixed.type() != Matchable::Type::Point)){
        omega_d += constraint_weight;
      }
      constraint.error_d=e_d.transpose()*omega_d*e_d;
      if(constraint.error_d>_direction_kernel_threshold) {
        real scale=sqrt(_direction_kernel_threshold/constraint.error_d);
        omega_d*=scale;
        is_inlier=false;
      }
      H_temp+=J_d.transpose()*omega_d*J_d;
      b_temp+=J_d.transpose()*omega_d*e_d;
    }

    //std::cerr << "D:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;
    // handle the orthogonality term
    if (entry.compute_o){
      real omega_o=_omega_o;
      if(_use_support_weight  && (fixed.type() != Matchable::Type::Point)){
        omega_o += constraint_weight(0,0);
      }
      constraint.error_o=e_o*omega_o*e_o;
      if(constraint.error_o>_orthogonal_kernel_threshold) {
        real scale=sqrt(_orthogonal_kernel_threshold/constraint.error_o);
        omega_o*=scale;
        is_inlier=false;
      }
      H_temp+=J_o.transpose()*omega_o*J_o;
      b_temp+=J_o.transpose()*omega_o*e_o;
    }
    //    std::cerr << "O:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;
    // handle the intersection term
    if (entry.compute_x){
      constraint.error_x=e_x*_omega_x*e_x;
      real omega_x=_omega_x;
      if(constraint.error_x>_intersection_kernel_threshold) {
        real scale=sqrt(_intersection_kernel_threshold/constraint.error_x);
        omega_x*=scale;
        is_inlier=false;
      }
      H_temp+=J_x.transpose()*omega_x*J_x;
      b_temp+=J_x.transpose()*omega_x*e_x;
    }
    //    std::cerr << "X:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;

    //    std::cerr << "CRISTO:"<<std::endl << _H << std::endl << _b.transpose() << std::endl;
    if (is_inlier || !_ignore_outliers){
      
      _H+=H_temp;
      _b+=b_temp;

      _num_inliers++;
      _chi_square += constraint.error_p + constraint.error_d + constraint.error_o + constraint.error_x;
    }
    //    std::cerr << "I/O:"<<std::endl << _H << std::endl << _b.transpose() << std::endl;
  }

  void IterativeRegistrationSolver::init(const Isometry3& transform_,
                                         const bool use_support_weight) {
    BaseRegistrationSolver::init(transform_, use_support_weight);
    _H.setZero();
    _b.setZero();
  }
  
  void IterativeRegistrationSolver::oneRound(){
    _H.setZero();
    _b.setZero();

    _num_inliers = 0;
    _chi_square = 0;

    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H += Matrix6::Identity() * _damping;
    Vector6 dx = _H.ldlt().solve(-_b);
    _T = srrg_core::v2tEuler(dx)*_T;
  }

} //ia end namespace srrg_bagasha
