#include <tracker/shape_tracker.h>

namespace srrg_sashago {

  size_t ShapeTracker::_vertex_id_generator = 0;

  ShapeTracker::ShapeTracker() {
    _global_T     = Isometry3::Identity();
    _inv_global_T = Isometry3::Identity();
    _aligner      = new ShapesAligner();
  }

  ShapeTracker::~ShapeTracker() {
    delete _aligner;
  }

  void ShapeTracker::init() {
    // ia placeholder
  }

  void ShapeTracker::compute(const Isometry3& guess_) {
    // ia check that there is at least one scene
    if (!_moving_scene)
      throw std::runtime_error(
        "[BagashaTracker::compute] did you forget to call \"setMovingScene()\"?");

    double t0 = 0;

    // ia here we will initialize the loop closure detector swiss guy
    // ia in this case we are in the first pose
    if (_moving_scene->sceneID() == 0) {
      _global_T = _moving_scene->gtPose();
      _moving_scene->setPose(_global_T);

      // ia add all the entries in the map
      for (size_t i = 0; i < _moving_scene->entries().size(); ++i) {
        _map->addEntry(_moving_scene->entries()[i]);
      }

      // ia g2o stuff for the first vertex
      g2o::VertexSE3EulerPert* v_pose = new g2o::VertexSE3EulerPert();
      v_pose->setId(_vertex_id_generator++);
      v_pose->setEstimate(_moving_scene->pose().cast<number_t>());
      v_pose->setFixed(true);
      _optimizer->addVertex(v_pose);
      _moving_scene->setPoseVertex(v_pose);

      _prev_pose_vertex = v_pose;

      _moving_scene = 0;
      return;
    }

    assert(_map->numEntries() && "[BagashaTracker::compute]| no entries in the map");

    // ia update the pool with all the previous landmark
    _map->updatePool();

    // ia compute the next globalT from the last known one
    _aligner->setFixedMap(_map);
    _aligner->setMovingScene(_moving_scene);
    _aligner->compute(_global_T * guess_);

    // ia update the globalT
    _global_T = _aligner->T();

    // ia add a pose to the moving scene
    _moving_scene->setPose(_global_T);

    // ia create a new pose vertex
    g2o::VertexSE3EulerPert* v_moving = new g2o::VertexSE3EulerPert();
    v_moving->setId(_vertex_id_generator++);
    v_moving->setEstimate(_global_T.cast<number_t>());
    _optimizer->addVertex(v_moving);
    _moving_scene->setPoseVertex(v_moving);

    // ia setup things for the next round
    _map->clearPool();
    for (size_t i = 0; i < _moving_scene->entries().size(); ++i) {
      SceneEntry* e      = _moving_scene->entries()[i];
      Landmark* landmark = _map->addEntry(e);

      if (!landmark)
        continue;

      // TODO check those guys if g2o graph is wrong
      // ia we have a landmark, check if it is new or old
      bool recursive_edge_generation           = false;
      g2o::HyperGraph::Vertex* landmark_vertex = landmark->vertexPtr();
      if (!landmark_vertex) {
        landmark_vertex = _generateMatchableVertex(e->globalMatchable());
        landmark->setVertexPtr(landmark_vertex);
        recursive_edge_generation = true;
      }
      g2o::HyperGraph::Edge* landmark_edge =
        _generateMatchableEdge(_moving_scene->poseVertex(), landmark_vertex, e->matchable());
      landmark->addEdge(landmark_edge);
      // bdc if landmark has been created now, we have to recursively generate the edges
      //    from the previous measurement of this landmark
      if (recursive_edge_generation && recursive_edge_generation) {
        SceneEntry* prev_e = e->prevSceneEntry();
        while (prev_e) {
          g2o::HyperGraph::Edge* landmark_prev_edge = _generateMatchableEdge(
            prev_e->scene()->poseVertex(), landmark_vertex, prev_e->matchable());
          landmark->addEdge(landmark_edge);
          prev_e = prev_e->prevSceneEntry();
        }
      }
    }

    // ia create a new pose edge between moving and fixed
    g2o::HyperGraph::Edge* pose_edge = _createPoseEdge(_prev_pose_vertex, v_moving);

    // ia move to next scene
    _prev_pose_vertex = v_moving;
    _moving_scene     = 0;
  }

  g2o::HyperGraph::Edge* ShapeTracker::_createPoseEdge(g2o::HyperGraph::Vertex* from_,
                                                       g2o::HyperGraph::Vertex* to_) {
    // fn new pose edge
    g2o::EdgeSE3ChordalErrorA* e = new g2o::EdgeSE3ChordalErrorA();
    e->vertices()[0]             = from_;
    e->vertices()[1]             = to_;
    e->information().setIdentity();
    e->setMeasurementFromState();
    _optimizer->addEdge(e);
    return e;
  }

  g2o::matchables::VertexMatchable* ShapeTracker::_generateMatchableVertex(const Matchable& m_) {
    const srrg_sashago::Matchable::Type& matchable_type = m_.type();

    // ia build the g2o matchable vertex
    g2o::matchables::Matchable::Type v_m_type;
    switch (matchable_type) {
      case (srrg_sashago::Matchable::Type::Point):
        v_m_type = g2o::matchables::Matchable::Type::Point;
        break;
      case (srrg_sashago::Matchable::Type::Line):
        v_m_type = g2o::matchables::Matchable::Type::Line;
        break;
      case (srrg_sashago::Matchable::Type::Plane):
        v_m_type = g2o::matchables::Matchable::Type::Plane;
        break;
      default:
        break;
    }

    g2o::matchables::Matchable graph_matchable(
      v_m_type, m_.point.cast<number_t>(), m_.rotation.cast<number_t>());

    g2o::matchables::VertexMatchable* v_m = new g2o::matchables::VertexMatchable();
    v_m->setId(_vertex_id_generator++);
    v_m->setEstimate(graph_matchable);

    _optimizer->addVertex(v_m);

    return v_m;
  }

  g2o::HyperGraph::Edge* ShapeTracker::_generateMatchableEdge(g2o::HyperGraph::Vertex* pose_v_,
                                                              g2o::HyperGraph::Vertex* matchable_v,
                                                              const Matchable& matchable_meas_) {
    g2o::VertexSE3EulerPert* vp = dynamic_cast<g2o::VertexSE3EulerPert*>(pose_v_);
    if (!vp)
      throw std::runtime_error("[BagashaTracker::generateMatchableEdge] nullptr on vp");
    g2o::matchables::VertexMatchable* vm =
      dynamic_cast<g2o::matchables::VertexMatchable*>(matchable_v);
    g2o::HyperGraph::Edge* e = 0;

    switch (vm->estimate().type()) {
      case g2o::matchables::Matchable::Type::Point:
        e = _computePointEdge(vp, vm, matchable_meas_);
        break;
      case g2o::matchables::Matchable::Type::Line:
        e = _computeLineEdge(vp, vm, matchable_meas_);
        break;
      case g2o::matchables::Matchable::Type::Plane:
        e = _computePlaneEdge(vp, vm, matchable_meas_);
        break;
      default:
        throw std::runtime_error(
          "[BagashaTracker::generateMatchableEdge] unexepected matchable type");
    }

    if (e) {
      _optimizer->addEdge(e);
    }

    return e;
  }

  g2o::HyperGraph::Edge* ShapeTracker::_computePointEdge(g2o::VertexSE3EulerPert* vfrom_,
                                                         g2o::matchables::VertexMatchable* vto_,
                                                         const Matchable& m_) {
    const g2o::matchables::Matchable& matchable = vto_->estimate();

    g2o::matchables::Matchable measurement(g2o::matchables::Matchable::Type::Point,
                                           m_.point.cast<number_t>(),
                                           m_.rotation.cast<number_t>());

    g2o::Matrix7 omega      = g2o::Matrix7::Zero();
    omega.block<3, 3>(0, 0) = matchable.omega();
    //    omega = omega*100.0;

    // TODO some consistency checks could be done here and return NULL if something is wrong
    // if (measurement_is_not_valid) return 0

    g2o::matchables::EdgeSE3Matchable* e = new g2o::matchables::EdgeSE3Matchable();
    e->vertices()[0]                     = vfrom_;
    e->vertices()[1]                     = vto_;
    e->setInformation(omega);
    e->setMeasurement(measurement);

    return e;
  }

  g2o::HyperGraph::Edge* ShapeTracker::_computeLineEdge(g2o::VertexSE3EulerPert* vfrom_,
                                                        g2o::matchables::VertexMatchable* vto_,
                                                        const Matchable& m_) {
    const g2o::matchables::Matchable& matchable = vto_->estimate();

    // ia build the g2o matchable vertex
    g2o::matchables::Matchable measurement(g2o::matchables::Matchable::Type::Line,
                                           m_.point.cast<number_t>(),
                                           m_.rotation.cast<number_t>());

    g2o::Matrix7 omega      = g2o::Matrix7::Zero();
    omega.block<3, 3>(0, 0) = matchable.omega();
    omega.block<3, 3>(3, 3) = g2o::Matrix3::Identity();
    //    omega = omega*100.0;

    // TODO some consistency checks could be done here and return NULL if something is wrong
    // if (measurement_is_not_valid) return 0

    g2o::matchables::EdgeSE3Matchable* e = new g2o::matchables::EdgeSE3Matchable();
    e->vertices()[0]                     = vfrom_;
    e->vertices()[1]                     = vto_;
    e->setInformation(omega);
    e->setMeasurement(measurement);

    return e;
  }

  g2o::HyperGraph::Edge* ShapeTracker::_computePlaneEdge(g2o::VertexSE3EulerPert* vfrom_,
                                                         g2o::matchables::VertexMatchable* vto_,
                                                         const Matchable& m_) {
    const g2o::matchables::Matchable& matchable = vto_->estimate();

    // ia build the g2o matchable vertex
    g2o::matchables::Matchable measurement(g2o::matchables::Matchable::Type::Plane,
                                           m_.point.cast<number_t>(),
                                           m_.rotation.cast<number_t>());

    g2o::Matrix7 omega      = g2o::Matrix7::Zero();
    omega.block<3, 3>(0, 0) = matchable.omega();
    omega.block<3, 3>(3, 3) = g2o::Matrix3::Identity();
    //    omega = omega*100.0;

    g2o::matchables::EdgeSE3Matchable* e = new g2o::matchables::EdgeSE3Matchable();
    e->vertices()[0]                     = vfrom_;
    e->vertices()[1]                     = vto_;
    e->setInformation(omega);
    e->setMeasurement(measurement);

    return e;
  }

} // namespace srrg_sashago
