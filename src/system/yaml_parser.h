#pragma once
#include <yaml-cpp/yaml.h>
#include "types/defs.h"

namespace srrg_sashago {

  class SashagoSystem;
  class ShapeDetector;
  class ShapeTracker;
  class Relocalizer;
  class Relocalizer;
  class LoopCloser;
  class TxtioParser;
  
  class YamlParser {
  public:

    struct Config {
      std::string configuration_name;
      std::string input_yaml_filename;
      std::string output_yaml_filename;
      SashagoSystem* system;
      ShapeDetector* detector;
      ShapeTracker* tracker;
      Relocalizer* relocalizer;
      LoopCloser* loop_closer;
      TxtioParser* txtio_parser;
           
      Config() {
        configuration_name   = "";
        input_yaml_filename  = "";
        output_yaml_filename = "out_configs.yaml";
        system       = 0;
        detector     = 0;
        tracker      = 0;
        relocalizer  = 0;
        loop_closer  = 0;
        txtio_parser = 0;
      }
    };
    
    YamlParser();
    virtual ~YamlParser();

    inline Config& mutableConfig() {return _configuration;}
    inline const Config& config() const {return _configuration;}
    
    //! @brief reads the current input_yaml_file and set the object (if !NULL)
    void read();

    //! @brief saves the current configuration in a yaml file - ia SUPER SHITTY DIOCAN
    void write();

  protected:
    //! @brief aux functions
    void _readSystemConfig();
    void _readTxtioParserConfig();
    void _readDetectorConfig();
    void _readTrackerConfig();
    void _readRelocalizerConfig();
    void _readLoopCloserConfig();

    void _writeSystemConfig(YAML::Emitter& emitter);
    void _writeDetectorConfig(YAML::Emitter& emitter);
    void _writeTrackerConfig(YAML::Emitter& emitter);
    void _writeRelocalizerConfig(YAML::Emitter& emitter);
    void _writeLoopCloserConfig(YAML::Emitter& emitter);
    void _writeTxtIOParserConfig(YAML::Emitter& emitter);
    
    Config _configuration;
    YAML::Node _external_configuration;    
    
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

}
