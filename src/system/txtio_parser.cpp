#include "txtio_parser.h"
#include "types/defs.h"
#include <iostream>

using namespace srrg_core;

namespace srrg_sashago {

  TxtioParser::TxtioParser() {
    _reader = new MessageReader(TXTIO);
    _synchronizer = new MessageTimestampSynchronizer();
    _timestamp = -1.0;
    _camera_matrix.setIdentity();
    _is_initialized = false;
    _message_number = 0;
  }

  TxtioParser::~TxtioParser() {
    delete _synchronizer;
    delete _reader;
    if(_writer) {
      _writer->close();
      delete _writer;
    }
    
    _is_initialized = false;
    _message_number = 0;
  }

  void TxtioParser::init() {
    assert(_reader && _synchronizer && "[TxtioParser::init]| bad things happened");

    if (_config.dataset_filename == "")
      throw std::runtime_error("[TxtioParser::init]| please set a valid dataset filename");
    if (_config.topic_depth == "" || _config.topic_rgb == "")
      throw std::runtime_error("[TxtioParser::init]| please set valid topics for depth and rgb data");

    if (_config.verbosity > VerbosityLevel::None) {
      std::cerr << "[TxtioParser::init]| topic depth: " << FG_YELLOW(_config.topic_depth) << std::endl;
      std::cerr << "[TxtioParser::init]| topic rgb  : " << FG_YELLOW(_config.topic_rgb) << std::endl;
    }

    std::vector<std::string> depth_plus_rgb_topic;
    depth_plus_rgb_topic.push_back(_config.topic_depth);
    depth_plus_rgb_topic.push_back(_config.topic_rgb);

    _camera_matrix.setIdentity();

    //ia for odom
    _gt_pose.setIdentity();
    _camera_guess.setIdentity();
    _prev_odom_reading.setIdentity();

    //ia everything is ok, initialize streamer guys
    _reader->open(_config.dataset_filename);
    _synchronizer->setTopics(depth_plus_rgb_topic);
    _synchronizer->setTimeInterval(_config.syncronizer_timestep);

    _is_initialized = true;
    _message_number = 0;
  }

  const TxtioParser::ParserStatus TxtioParser::processData() {
    if (!_is_initialized)
      throw std::runtime_error("[TxtioParser::processData]|did you forgot to call \'init()\'?");

    BaseMessage* msg_base = _reader->readMessage();
    if (!msg_base)
      return ParserStatus::Stop;

    BaseImageMessage* msg_img = dynamic_cast<BaseImageMessage*>(msg_base);
    if (!msg_img) {
      return ParserStatus::Unsync;
    }

    _synchronizer->putMessage(msg_img);
    if (!_synchronizer->messagesReady())
      return ParserStatus::Unsync;

    if (_config.verbosity > VerbosityLevel::None)
      std::cerr << "[TxtioParser::processData]| message number: " << FG_YELLOW(_message_number) << std::endl;

    //ia cast things
    PinholeImageMessage* msg_img_depth = dynamic_cast<PinholeImageMessage*>(_synchronizer->messages()[0].get());;
    PinholeImageMessage* msg_img_rgb = dynamic_cast<PinholeImageMessage*>(_synchronizer->messages()[1].get());
    assert(msg_img_depth && msg_img_rgb && "[TxtioParser::processData]| unexpected error, exit");

    //ia copy images
    msg_img_depth->image().copyTo(_image_depth);
    msg_img_rgb->image().copyTo(_image_rgb);

    _timestamp = msg_img_rgb->timestamp();

    if (msg_img_rgb->hasOdom()) {
      _camera_guess = msg_img_rgb->offset().inverse() * _prev_odom_reading.inverse() * msg_img_rgb->odometry() * msg_img_rgb->offset();
      _gt_pose = _gt_pose * _camera_guess;
      _prev_odom_reading = msg_img_rgb->odometry();
    } else {
      _gt_pose.setIdentity();
      _camera_guess.setIdentity();
    }

    if (!_message_number) {
      _camera_matrix = msg_img_depth->cameraMatrix();
    }

    ++_message_number;
    return ParserStatus::Good;
  }


  void TxtioParser::write(IntSceneMap* scenes_) {
    if(_config.output_filename.empty())
      throw std::runtime_error("[TxtioParser::write]| provided output file for txtio is empty");
    if(!_writer)
      _writer = new MessageWriter(TXTIO);
    _writer->open(_config.output_filename);
    //bdc reset the reader
    _reader->close();
    _reader->open(_config.dataset_filename);
    _synchronizer->reset();
    _synchronizer->setTimeInterval(_config.syncronizer_timestep);
    
    while(_reader->good()) {
      BaseMessage* msg_base = _reader->readMessage();
      if (!msg_base)
        continue;

      BaseImageMessage* msg_img = dynamic_cast<BaseImageMessage*>(msg_base);
      assert(msg_img && "[TxtioParser::write]| unexpected error, exit");

      _synchronizer->putMessage(msg_img);
      if (!_synchronizer->messagesReady())
        continue;

      //ia cast things
      PinholeImageMessage* msg_img_depth = dynamic_cast<PinholeImageMessage*>(_synchronizer->messages()[0].get());;
      PinholeImageMessage* msg_img_rgb = dynamic_cast<PinholeImageMessage*>(_synchronizer->messages()[1].get());
      assert(msg_img_depth && msg_img_rgb && "[TxtioParser::write]| unexpected error, exit");

      double timestamp = msg_img_rgb->timestamp();
      uint64_t closest_scene = std::numeric_limits<uint64_t>::max();
      double closest_time_distance = std::numeric_limits<double>::max();
      //bdc get closest (in time) transform
      for(const IntScenePair& pair : *scenes_) {
        double time_distance = fabs(pair.second->timeStamp() - timestamp);
        if(time_distance < closest_time_distance) {
          closest_time_distance = time_distance;
          closest_scene = pair.first;
        }
      }
      //bdc if too far in time, continue
      if(closest_time_distance < _config.syncronizer_timestep) {
        auto corresponding_pair_entry = scenes_->find(closest_scene);
        if(corresponding_pair_entry != scenes_->end()) {
          const Isometry3& odometry = corresponding_pair_entry->second->pose();
          msg_img_depth->setOdometry(odometry);
          _writer->writeMessage(*msg_img_depth);          
          msg_img_rgb->setOdometry(odometry);
          _writer->writeMessage(*msg_img_rgb);
        }
      }      
    } // while
    
    _writer->close();
  }
  
} //ia end namespace srrg_bagasha
