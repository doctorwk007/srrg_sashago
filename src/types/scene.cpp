#include "scene.h"

namespace srrg_sashago {

  uint64_t Scene::_id_generator = 0;

  Scene::Scene() {
    _pose = Eigen::Isometry3f::Identity();
    _gt_pose = Eigen::Isometry3f::Identity();
    _timestamp = -1;
    _id = _id_generator++;
    _is_pose_set = false;
  }

  Scene::~Scene() {
    clear();
  }

  void Scene::clear(const bool& destroy_) {
    _pose = Eigen::Isometry3f::Identity();
    _gt_pose = Eigen::Isometry3f::Identity();
    _timestamp = -1;
    _id = 0;

    if (destroy_) {
      for (size_t i = 0; i < _scene_entries.size(); ++i) {
        delete _scene_entries[i];
      }
    }
    _scene_entries.clear();
    _is_pose_set = false;
  }

  void Scene::setPose(const Isometry3& pose_) {
    _pose = pose_;
    for (size_t i = 0; i < _scene_entries.size(); ++i) {
      _scene_entries[i]->_computeGlobalMatchable();
    }
    _is_pose_set = true;
  }


  SceneEntry* Scene::addEntry(const Matchable& matchable_) {
    SceneEntry* new_entry = new SceneEntry(matchable_, this);
    _scene_entries.push_back(new_entry);
    return new_entry;
  }

} //ia end namespace
